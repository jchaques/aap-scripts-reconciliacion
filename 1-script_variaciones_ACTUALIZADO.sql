﻿﻿﻿
-- variaciones de MGC a ADM_RECON

-- Truncar tabla variaciones de ADM_RECON, las 3: mgc_variaciones_pdte, mgc_variaciones_hist y mgc_variaciones_urg

-- Select a Variaciones de MGC con las siguientes querys, para de seguido exportar (en inserts) y insertarlo en ADM_RECON

-- Modificar fec_valor por fecha de la foto

select * from mgc_variaciones_hist where hv_cod_producto in (select m_merca_uom from s_articulo_merca where fec_d_alta = '01/01/2021')
and hv_fec_valor > '16/01/2020';-- ok

-------------------------------------------

select * from mgc_variaciones_pdte where var_cod_producto in (select m_merca_uom from s_articulo_merca where fec_d_alta = '01/01/2021')
and var_fec_valor > '16/01/2020';--ok

--------------------------------------------

select * from mgc_variaciones_urg where vu_cod_producto in (select m_merca_uom from s_articulo_merca where fec_d_alta = '01/01/2021')
and vu_fec_valor > '16/01/2020';--ok

--Si hay un cambio urgente incluirlo MANUALMENTE en el email a Fernando/Pablo/Ana

-------------------------------

-- REVISAR PROVEEDORES (FILTRADO YA POR EL ALCANCE)

select * from VALIDACION_PROVEEDORES;

-- REVISAR CENTROS

-- Revisar d_Centro_extendida_vm que copia Sothis (incluir en clonar _2)

/* Revisar cierres/reformas/aperturas */

select ce_cod_centro, ce_fec_apertura, ce_fec_cierre, ce_tienda_abierta, CE_SURTIDO_VALIDADO from mgc_centros

minus

select ce_cod_centro, ce_fec_apertura, ce_fec_cierre, ce_tienda_abierta, CE_SURTIDO_VALIDADO  from mgc_centros_2;


/* Datos Centros (Para florituras de Fernando de precios) */

/* Truncar table de adm_recon datos_centros */

-- DE MGC O DE TRART PRE

select numero_centro, codigo_postal from TRART_CENTRO_EXT_MVIEW_MAESTRA;--ok

---------------------------------------------------




--  ********************** PRODUCTO ******************************



-- 1. Copiamos mgc_variaciones_pdte, mgc_variaciones_hist y mgc_variaciones_urg de MGC PRE a ADM_RECON PRO (query adjuntada arriba) (inserts de toda la vida)

-- 2. Actualizamos las vistas materializas, de uno en uno: click derecho "Opción de Refrescamiento" - "Forzar Refrescamiento Ahora" - Tipo C y "Aplicar"
-- OJO: La de Surtido tarda mas en refrescarse que las otras

-- 3. Lanzamos select * from variaciones_productos; (vista) para ver las variaciones que pasaremos a Ana (LO QUE NO ESTA CONSOLIDADO EN TRART)

select * from variaciones_productos;

-- 4. Lanzamos: select * from vw_b_sap_mara para ver que productos han cambiado (CONSOLIDADO EN TRART) -> Abrir Excel "Validacion_MARA_Migracion.xlsx" (Hay un ejemplo en el excel)

select * from vw_b_sap_mara;

-- 4.1 Copiamos regitros en FOTO 1 PRODUCCION

SELECT M_MERCA,MBRSH,MATKL,MEINS,NTGEW,GEWEI,VOLUM,VOLEH,TRAGR,PRDHA,DATAB,LIQDT,ATTYP,MTPOS_MARA,BRAND_ID,
GPA_CLASS,MPK_PRIMAL,MPK_YIELD,M_GRANEL,M_FORMATO_USOS,M_UM_FORMATO_USOS,M_TIPO_VENTA_PRODUCTO,M_PLAZO_PAGO,M_SE_EDITAN_ETIQUETAS,
M_LIQUIDO_APROVECHABLE,M_TARA,M_PESO_FIJO,M_RECOMENDADO,M_TIPO_PEDIDO_PRODUCTO,M_TIPO_VENTA_BALANZA,M_FORMATO_VENTA,M_UM_FORMATO_VENTA,
M_PESO_NETO_ESCURRIDO,M_TIPO_SUMINISTRO,M_DIAS_VIDA_GARANTIZADOS,M_CANTIDAD_FTO_LEGAL_ETIQ,M_NEGOCIO,M_NECESIDAD,M_CATEGORIA,M_SUBCATEGORIA,
M_UM_FTO_LEGAL,M_TIPO_ETIQ,M_SECCION,M_FAMILIA,M_SUBFAMILIA,M_VARIEDAD,M_HAY_ASISTENCIA_VENTA,M_DENOMINACION_CIENTIFICA,M_ES_PRODUCTO_CAMPANYA,
M_UM_TARA,M_ENVIO_BALANZA,M_IMPLANTADO_REING,M_DESCRIP_ETIQUETA_ES,M_TIPO_TRANSFORMACION,M_TIPO_VENTA_NV,M_ENVOLTORIO,M_UM_MOVIMIENTO,NRFHG,
M_NUM_PORCENTAJE_MERMA,PROD_HIER
FROM B_SAP_MARA WHERE M_MERCA IN (SELECT M_MERCA_UOM FROM S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021');

-- 4.2 Copiamos registros en FOTO _2 MIGRACIÓN

SELECT M_MERCA,MBRSH,MATKL,MEINS,NTGEW,GEWEI,VOLUM,VOLEH,TRAGR,PRDHA,DATAB,LIQDT,ATTYP,MTPOS_MARA,BRAND_ID,
GPA_CLASS,MPK_PRIMAL,MPK_YIELD,M_GRANEL,M_FORMATO_USOS,M_UM_FORMATO_USOS,M_TIPO_VENTA_PRODUCTO,M_PLAZO_PAGO,M_SE_EDITAN_ETIQUETAS,
M_LIQUIDO_APROVECHABLE,M_TARA,M_PESO_FIJO,M_RECOMENDADO,M_TIPO_PEDIDO_PRODUCTO,M_TIPO_VENTA_BALANZA,M_FORMATO_VENTA,M_UM_FORMATO_VENTA,
M_PESO_NETO_ESCURRIDO,M_TIPO_SUMINISTRO,M_DIAS_VIDA_GARANTIZADOS,M_CANTIDAD_FTO_LEGAL_ETIQ,M_NEGOCIO,M_NECESIDAD,M_CATEGORIA,M_SUBCATEGORIA,
M_UM_FTO_LEGAL,M_TIPO_ETIQ,M_SECCION,M_FAMILIA,M_SUBFAMILIA,M_VARIEDAD,M_HAY_ASISTENCIA_VENTA,M_DENOMINACION_CIENTIFICA,M_ES_PRODUCTO_CAMPANYA,
M_UM_TARA,M_ENVIO_BALANZA,M_IMPLANTADO_REING,M_DESCRIP_ETIQUETA_ES,M_TIPO_TRANSFORMACION,M_TIPO_VENTA_NV,M_ENVOLTORIO,M_UM_MOVIMIENTO,NRFHG,
M_NUM_PORCENTAJE_MERMA,PROD_HIER
FROM B_SAP_MARA_2 WHERE M_MERCA IN (SELECT M_MERCA_UOM FROM S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021')

-- 4.3 Vemos diferencias y se las pasamos Ana, en rojo lo que ha cambiado.

-- 5. Comprobar que no hay duplicidad y todo cuadra

-- 6. Doble comprobación:

-- 6.1 Cruzar minus con variaciones (lo que no esté en variaciones)

select * from vw_b_sap_mara where m_merca not in (select m_merca from variaciones_productos);

-- 6.2 Tirar Scripts Reconciliación de Productos sin "_2" (opcional)



----------------------------------------------------------


-- ************************ SURTIDO **********************




-- 1. SELECT A LA VISTA VARIACIONES_SURTIDO (vista) -> PASAR A PABLO

-- 1.1 Revisar fecha valor de la vista

select * from variaciones_surtido_2;


-- 2. SELECT A LA VISTA DE VARIACIONES_SURTIDO_ALMACEN -> PASAR A PABLO

select * from VARIACIONES_SURTIDO_ALMACEN;

-- 2.1 Ver variaciones surtido almacen servicio externo en la vista (ver que No se duplican en la otra vista)

select * from VARIACIONES_SURT_ALM_PLAN;

-- 2.2 Ver las fechas inicio y fin del surtido almacen filtrando por mercas 

select distinct t2.m_merca,t2.fec_d_inicio, t2.fec_d_fin,t3.cod_v_site_sap
from b_sap_surtido t2
left join d_centro_extendida_vm t3 on (t3.numero_centro = t2.txt_nombre_corto_sap)
where m_merca in (SELECT m_merca_uom FROM s_articulo_merca where fec_d_alta = '01/01/2021')
and m_merca in ('XXXX') and t3.cod_v_site_sap in ('XXXX');


-- 3. Comprobar que no hay duplicidad y todo cuadra:

/* Surtido de Variaciones pillado por MGC y el consolidado en TRART, excluyendo al surtido que ya esta TRART 
(para NO pasarle algo que ya este hecho) */

SELECT m_merca, fec_d_inicio, fec_d_fin, cod_v_site_sap FROM VARIACIONES_SURTIDO_2

minus

select distinct t2.m_merca,t2.fec_d_inicio, t2.fec_d_fin,t3.cod_v_site_sap
from b_sap_surtido_2 t2
left join d_centro_extendida_vm t3 on (t3.numero_centro = t2.txt_nombre_corto_sap)
where m_merca in (SELECT m_merca_uom FROM s_articulo_merca where fec_d_alta = '01/01/2021');


---------------------------------------------------------------


-- ****************** PRECIO ***********************

-- 0. Actualizamos las vistas: VARIACIONES_PCB_ALL (trart), VARIACIONES_PCB_MGC, VARIACIONES_PVP_ALL (trart) Y VARIACIONES_PVP_MGC

-- 1. Para ver los cambios consolidados en trart tiramos la siguiente consulta: exportar a excel

select * from variaciones_pcb_all order by to_date(valid_from,'DD/MM/YYYY') asc;

-- 2. Para ver los cambios NO consolidados en TRART (variaciones pendientes MGC):

select * from variaciones_pcb_mgc order by to_date(date_ini,'DD/MM/YYYY') asc;

-- 2.1 Comprobamos que no hayan duplicados: exportar a excel

select * from variaciones_pcb_mgc where material NOT in (SELECT trim(matnr) FROM VARIACIONES_PVP_all) order by to_date(date_ini,'DD/MM/YYYY') asc;

-- 3. Para los cambios consolidados en TRART de PVP:

select * from variaciones_pvp_all;

-- 3.1 Para los cambios NO consolidados en TRART:

select * from variaciones_PVP_mgc where MATNR NOT in (SELECT trim(matnr) FROM VARIACIONES_PVP_all) order by to_date(date_ini,'DD/MM/YYYY') asc;


-- 4. Comprobar que no hay duplicidad y todo cuadra

-- 4.2 Ver si hay arrepentimientos (filtrar por duplicados y fijarse en la fecha de creacion)


-------------------------------------------------
--Truncar para liberar espacio	
truncate table B_SAP_DENOM_RECON_POST;
truncate table B_SAP_DENOM_RECON_PRE;
truncate table B_SAP_EAN_PROV_RECON_POST;
truncate table B_SAP_EAN_PROV_RECON_PRE;
truncate table B_SAP_EAN_RECON_POST;
truncate table B_SAP_EAN_RECON_PRE;
truncate table B_SAP_PCB_RECON_POST;
truncate table B_SAP_PCB_RECON_PRE;
truncate table B_SAP_PROD_IMP_RECON_POST;
truncate table B_SAP_PROD_LOC_RECON_POST;
truncate table B_SAP_PROD_LOC_RECON_PRE;
truncate table B_SAP_PROD_RECON_POST;
truncate table B_SAP_PROD_RECON_PRE;
truncate table B_SAP_PVP_RECON_POST;
truncate table B_SAP_PVP_RECON_PRE;
truncate table B_SAP_SURTIDO_RECON_POST;
truncate table B_SAP_SURTIDO_RECON_PRE;
truncate table B_SAP_SURT_PRV_RECON_POST;
truncate table B_SAP_SURT_PRV_RECON_PRE;
truncate table B_SAP_UOM_RECON_POST;
truncate table B_SAP_UOM_RECON_PRE;
truncate table B_SAP_INFORME_RECONCILIACION;


--Clonar para poder comparar
create table B_SAP_ADDITIONAL_2 as select * from B_SAP_ADDITIONAL; 
create table B_SAP_CLASIFICACION_FISCAL_2 as select * from B_SAP_CLASIFICACION_FISCAL; 
create table B_SAP_EINA_2 as select * from B_SAP_EINA; 
create table B_SAP_EINE_2 as select * from B_SAP_EINE; 
create table B_SAP_LOCALISMO_2 as select * from B_SAP_LOCALISMO; 
create table B_SAP_MAKT_2 as select * from B_SAP_MAKT; 
create table B_SAP_MAMT_2 as select * from B_SAP_MAMT; 
create table B_SAP_MARA_2 as select * from B_SAP_MARA; 
create table B_SAP_MARM_2 as select * from B_SAP_MARM; 
create table B_SAP_MATERIAL_IMPUESTO_2 as select * from B_SAP_MATERIAL_IMPUESTO; 
create table B_SAP_MEAN_2 as select * from B_SAP_MEAN; 
create table B_SAP_MERCA_EXCLUSION_2 as select * from B_SAP_MERCA_EXCLUSION; 
create table B_SAP_MLEA_2 as select * from B_SAP_MLEA; 
create table B_SAP_PCB_ARTICLE_2 as select * from B_SAP_PCB_ARTICLE; 
create table B_SAP_PCB_ARTICLE_SITE_2 as select * from B_SAP_PCB_ARTICLE_SITE; 
create table B_SAP_PVP_ARTICLE_2 as select * from B_SAP_PVP_ARTICLE; 
create table B_SAP_PVP_ARTICLE_PRICELIST_2 as select * from B_SAP_PVP_ARTICLE_PRICELIST; 
create table B_SAP_PVP_ARTICLE_SITE_2 as select * from B_SAP_PVP_ARTICLE_SITE; 
create table B_SAP_SURTIDO_2 as select * from B_SAP_SURTIDO; 
create table B_SAP_TNTP_2 as select * from B_SAP_TNTP; 
create table B_SAP_T002_2 as select * from B_SAP_T002; 
create table B_SAP_T006_2 as select * from B_SAP_T006; 
create table B_SAP_T134_2 as select * from B_SAP_T134; 
create table B_SAP_WRF_BRANDS_2 as select * from B_SAP_WRF_BRANDS; 
create table C_SAP_CONF_ART_TYPE_CATEGORY_2 as select * from C_SAP_CONF_ART_TYPE_CATEGORY; 
create table C_SAP_CONF_PRECIOS_AGRUP_2 as select * from C_SAP_CONF_PRECIOS_AGRUP; 
create table C_SAP_CONF_TIPO_ETIQUETA_2 as select * from C_SAP_CONF_TIPO_ETIQUETA; 
create table C_TRART_PARAMETROS_2 as select * from C_TRART_PARAMETROS; 
create table C_TRF_EXCEPCION_2 as select * from C_TRF_EXCEPCION; 
create table C_TRF_MATRIZ_CONF_2 as select * from C_TRF_MATRIZ_CONF; 
create table ITF1_PRODUCTOS_PILOTO_VM_2 as select * from ITF1_PRODUCTOS_PILOTO_VM; 
create table MGC_CENTRO_PRODUCTO_2 as select * from MGC_CENTRO_PRODUCTO; 
create table MGC_CENTROS_2 as select * from MGC_CENTROS; 
create table MGC_CODINT_2 as select * from MGC_CODINT; 
create table MGC_COMPONENTES_2 as select * from MGC_COMPONENTES; 
create table MGC_DENOM_PROD_IDIOMAS_2 as select * from MGC_DENOM_PROD_IDIOMAS; 
create table MGC_EAN_PROD_2 as select * from MGC_EAN_PROD; 
create table MGC_EAN_PROD_PROV_2 as select * from MGC_EAN_PROD_PROV; 
create table MGC_PROD_TARA_2 as select * from MGC_PROD_TARA; 
create table MGC_PROD_TARIFAS_2 as select * from MGC_PROD_TARIFAS; 
create table MGC_PROD_TIPO_IMPUESTOS_2 as select * from MGC_PROD_TIPO_IMPUESTOS; 
create table MGC_PRODUCTOS_2 as select * from MGC_PRODUCTOS; 
create table MGC_SERVICIO_EXTERNO_2 as select * from MGC_SERVICIO_EXTERNO; 
create table MGC_SERVICIO_EXTERNO_HIST_2 as select * from MGC_SERVICIO_EXTERNO_HIST; 
create table MGC_SURTIDO_PROVEEDORES_2 as select * from MGC_SURTIDO_PROVEEDORES; 
create table MGC_TIPO_SUMINISTRO_SAM_2 as select * from MGC_TIPO_SUMINISTRO_SAM; 
create table S_TIPO_PRODUCTO_SAP_2 as select * from S_TIPO_PRODUCTO_SAP; 
create table S_TRANSFORMACIONES_2 as select * from S_TRANSFORMACIONES; 
create table T_ANDAMIO_NEGOCIO_2 as select * from T_ANDAMIO_NEGOCIO; 
create table T_SAP_MAPEO_ALMACENES_2 as select * from T_SAP_MAPEO_ALMACENES; 
create table T_SAP_MERCA_EXCEPCIONES_2 as select * from T_SAP_MERCA_EXCEPCIONES; 
create table T_TRF_FORMULA_2 as select * from T_TRF_FORMULA; 
create table T_TRF_SAP_BOM_2 as select * from T_TRF_SAP_BOM; 
create table T_TRF_SAP_PRODUCTO_2 as select * from T_TRF_SAP_PRODUCTO; 
create table D_CENTRO_EXTENDIDA_VM_2 as select * from D_CENTRO_EXTENDIDA_VM;
/* No Venta*/
create table mgc_productosnv_2 as select * from mgc_productosnv;
create table mgc_centro_producto_nv_2 as select * from mgc_centro_producto_nv;
create table MGC_EAN_PRODNV_2 as select * from MGC_EAN_PRODNV;
create table MGC_SERVICIO_EXTERNO_NV_2 as select * from MGC_SERVICIO_EXTERNO_NV;
create table MGC_SURTIDO_PROVEEDORES_NV_2 as select * from MGC_SURTIDO_PROVEEDORES_NV;
create table mgc_denom_prodnv_idiomas_2 as select * from mgc_denom_prodnv_idiomas;
create table mgc_ean_prodnv_prov_2 as select * from mgc_ean_prodnv_prov;

--truncar tablas renombradas XX_2
drop table MGC_PROD_TIPO_IMPUESTOS_2;
drop table MGC_DENOM_PROD_IDIOMAS_2;
drop table MGC_CENTROS_2;
drop table MGC_TIPOS_AGRUP_CENTRO_2;
drop table MGC_AGRUPACION_CENTRO_2;
drop table MGC_CENTRO_POR_AGRUPACION_2;
drop table B_SAP_ADDITIONAL_2;
drop table B_SAP_CLASIFICACION_FISCAL_2;
drop table B_SAP_EINA_2;
drop table B_SAP_EINE_2;
drop table B_SAP_LOCALISMO_2;
drop table B_SAP_MAKT_2;
drop table B_SAP_MAMT_2;
drop table B_SAP_MARM_2;
drop table B_SAP_MATERIAL_IMPUESTO_2;
drop table B_SAP_MEAN_2;
drop table B_SAP_MERCA_EXCLUSION_2;
drop table B_SAP_MLEA_2;
drop table B_SAP_PCB_ARTICLE_2;
drop table B_SAP_PCB_ARTICLE_SITE_2;
drop table MGC_PROD_TARIFAS_2;
drop table B_SAP_PVP_ARTICLE_2;
drop table B_SAP_PVP_ARTICLE_PRICELIST_2;
drop table B_SAP_PVP_ARTICLE_SITE_2;
drop table B_SAP_SURTIDO_2;
drop table B_SAP_TNTP_2;
drop table B_SAP_T002_2;
drop table B_SAP_T006_2;
drop table B_SAP_T134_2;
drop table B_SAP_WRF_BRANDS_2;
drop table C_SAP_CONF_ART_TYPE_CATEGORY_2;
drop table C_SAP_CONF_PRECIOS_AGRUP_2;
drop table C_SAP_CONF_TIPO_ETIQUETA_2;
drop table C_TRART_PARAMETROS_2;
drop table C_TRF_EXCEPCION_2;
drop table C_TRF_MATRIZ_CONF_2;
drop table ITF1_PRODUCTOS_PILOTO_VM_2;
drop table MGC_EAN_PROD_2;
drop table MGC_EAN_PROD_PROV_2;
drop table MGC_PRODUCTOS_2;
drop table MGC_SURTIDO_PROVEEDORES_2;
drop table MGC_CODINT_2;
drop table MGC_SERVICIO_EXTERNO_2;
drop table MGC_CENTRO_PRODUCTO_2;
drop table MGC_TIPO_SUMINISTRO_SAM_2;
drop table MGC_COMPONENTES_2;
drop table MGC_PROD_TARA_2;
drop table MGC_SERVICIO_EXTERNO_HIST_2;
drop table S_TRANSFORMACIONES_2;
drop table PRODUCTOS_2;
drop table B_SAP_STKO_2;
drop table MGC_VARIACIONES_URG_2;
drop table MGC_VARIACIONES_HIST_2;
drop table MGC_VARIACIONES_PDTE_2;
drop table D_CENTRO_EXTENDIDA_VM_2;
/* No Venta*/
drop table mgc_productosnv_2;
drop table mgc_centro_producto_nv_2;
drop table MGC_EAN_PRODNV_2;
drop table MGC_SERVICIO_EXTERNO_NV_2;
drop table MGC_SURTIDO_PROVEEDORES_NV_2
drop table mgc_denom_prodnv_idiomas_2;
drop table mgc_ean_prodnv_prov_2;




--------------------------------------------------------
--EINA:
select t2.cod_v_internalid_sap as proveedor_sap, t1.* from vw_b_sap_eina t1
left join S_TRAVE_PROVEEDORES_MVIEW t2 on (m_cod_prv = cod_n_proveedor_legacy);

--EINE:
select t2.cod_v_internalid_sap as proveedor_sap, t1.* from vw_b_sap_eine t1
left join S_TRAVE_PROVEEDORES_MVIEW t2 on (m_cod_prv = cod_n_proveedor_legacy);

--MARA:
select * from vw_b_sap_mara;

--MAKT:
select * from vw_b_sap_makt;

--MAMT:
select * from vw_b_sap_mamt;

--MARM:
select * from vw_b_sap_marm;

--B_SAP_MATERIAL_IMPUESTO:
select * from vw_b_sap_material_impuesto;

--MLEA
select * from VW_B_SAP_MLEA;

--PCB ARTICLE
select t2.cod_v_internalid_sap, T1.* from VW_B_SAP_PCB_ARTICLE T1
left join S_TRAVE_PROVEEDORES_MVIEW t2 on (m_cod_prv = cod_n_proveedor_legacy);

--PCB ARTICLE SITE:
select t2.cod_v_internalid_sap, T1.* from VW_B_SAP_PCB_ARTICLE_SITE T1
left join S_TRAVE_PROVEEDORES_MVIEW t2 on (m_cod_prv = cod_n_proveedor_legacy);

--PVP ARTICLE:
SELECT * FROM VW_B_SAP_PVP_ARTICLE;

--PVP ARTICLE PRICELIST
SELECT * FROM VW_B_SAP_PVP_ARTICLE_PRICELIST;

--VARIACIONES PVP (NIVEL PL,ARTICLE Y SITE) 
select * from variaciones_PVP_ALL;

--VARIACIONES PCB (NIVEL PL,ARTICLE Y SITE) 
select * from VARIACIONES_PCB_ALL;

-- VARIACIONES PCB NO CONSOLIDADAS DE MGC 	
SELECT * FROM VARIACIONES_PCB_MGC;
 
--VARIACIONES SURTIDO						
select * from VARIACIONES_SURTIDO;

-- VARIACIONES SURTIDO ALMACEN				
select * from VARIACIONES_SURTIDO_ALMACEN;

-- VARIACION PRODUCTOS												
select * from VARIACIONES_PRODUCTOS;
select count(*), Campo from variaciones_productos group by Campo;

-- Lo que no genera variación de productos
select * from vw_b_sap_mara where m_merca not in (select merca from variaciones_productos);
select * from vw_b_sap_mara; 
-------------------------------
--Foto produccion
SELECT M_MERCA, MATNR,MTART,MBRSH,MATKL,MEINS,NTGEW,GEWEI,VOLUM,VOLEH,TRAGR,PRDHA,DATAB,LIQDT,ATTYP,MTPOS_MARA,BRAND_ID,GPA_CLASS,MPK_PRIMAL,MPK_YIELD,M_GRANEL,M_FORMATO_USOS,M_UM_FORMATO_USOS,M_TIPO_VENTA_PRODUCTO,M_PLAZO_PAGO,M_SE_EDITAN_ETIQUETAS,M_LIQUIDO_APROVECHABLE,M_TARA	M_PESO_FIJO,M_RECOMENDADO,M_TIPO_PEDIDO_PRODUCTO,M_TIPO_VENTA_BALANZA,M_FORMATO_VENTA,M_UM_FORMATO_VENTA,M_PESO_NETO_ESCURRIDO,M_TIPO_SUMINISTRO,M_DIAS_VIDA_GARANTIZADOS,M_CANTIDAD_FTO_LEGAL_ETIQ,M_NEGOCIO,M_NECESIDAD,M_CATEGORIA,M_SUBCATEGORIA,M_UM_FTO_LEGAL,M_TIPO_ETIQ,M_SECCION,M_FAMILIA,M_SUBFAMILIA,M_VARIEDAD,M_HAY_ASISTENCIA_VENTA,M_DENOMINACION_CIENTIFICA,M_ES_PRODUCTO_CAMPANYA,M_UM_TARA,M_ENVIO_BALANZA,M_IMPLANTADO_REING,M_DESCRIP_ETIQUETA_ES,M_TIPO_TRANSFORMACION,M_TIPO_VENTA_NV,M_ENVOLTORIO,M_UM_MOVIMIENTO,NRFHG,M_NUM_PORCENTAJE_MERMA,PROD_HIER
FROM B_SAP_MARA WHERE M_MERCA IN (SELECT M_MERCA_UOM FROM S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021');
----------------------------
--Foto migracion
SELECT M_MERCA, MATNR,MTART,MBRSH,MATKL,MEINS,NTGEW,GEWEI,VOLUM,VOLEH,TRAGR,PRDHA,DATAB,LIQDT,ATTYP,MTPOS_MARA,BRAND_ID,GPA_CLASS,MPK_PRIMAL,MPK_YIELD,M_GRANEL,M_FORMATO_USOS,M_UM_FORMATO_USOS,M_TIPO_VENTA_PRODUCTO,M_PLAZO_PAGO,M_SE_EDITAN_ETIQUETAS,M_LIQUIDO_APROVECHABLE,M_TARA	M_PESO_FIJO,M_RECOMENDADO,M_TIPO_PEDIDO_PRODUCTO,M_TIPO_VENTA_BALANZA,M_FORMATO_VENTA,M_UM_FORMATO_VENTA,M_PESO_NETO_ESCURRIDO,M_TIPO_SUMINISTRO,M_DIAS_VIDA_GARANTIZADOS,M_CANTIDAD_FTO_LEGAL_ETIQ,M_NEGOCIO,M_NECESIDAD,M_CATEGORIA,M_SUBCATEGORIA,M_UM_FTO_LEGAL,M_TIPO_ETIQ,M_SECCION,M_FAMILIA,M_SUBFAMILIA,M_VARIEDAD,M_HAY_ASISTENCIA_VENTA,M_DENOMINACION_CIENTIFICA,M_ES_PRODUCTO_CAMPANYA,M_UM_TARA,M_ENVIO_BALANZA,M_IMPLANTADO_REING,M_DESCRIP_ETIQUETA_ES,M_TIPO_TRANSFORMACION,M_TIPO_VENTA_NV,M_ENVOLTORIO,M_UM_MOVIMIENTO,NRFHG,M_NUM_PORCENTAJE_MERMA,PROD_HIER
FROM B_SAP_MARA_2 WHERE M_MERCA IN (SELECT M_MERCA_UOM FROM S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021');


-- solo tiendas
select * from vw_b_sap_surtido where TXT_NOMBRE_CORTO_SAP >=2000;
select * from vw_mgc_centro_producto where cp_cod_centro >=2000;
-- solo almacenes
select * from vw_b_sap_surtido where TXT_NOMBRE_CORTO_SAP <2000;


--Tiendas
select * from vw_b_sap_surtido where txt_nombre_corto_sap>=2000 and fec_d_inicio>='07/10/2019' and fec_d_inicio<='13/10/2019';
select * from vw_b_sap_surtido where txt_nombre_corto_sap>=2000 and fec_d_fin>='07/10/2019' and fec_d_fin<='13/10/2019';
select * from vw_b_sap_surtido where txt_nombre_corto_sap>=2000 and fec_d_inicio>='14/10/2019';
select * from vw_b_sap_surtido where txt_nombre_corto_sap>=2000 and fec_d_fin>='14/10/2019';

--Tiendas
select * from vw_b_sap_surtido where txt_nombre_corto_sap>=2000 and fec_d_inicio>='07/10/2019' and fec_d_inicio<='13/10/2019';
select * from vw_b_sap_surtido where txt_nombre_corto_sap>=2000 and fec_d_inicio>='14/10/2019';

--Almacenes
select txt_nombre_corto_sap from b_sap_surtido where txt_nombre_corto_sap<2000 minus
select txt_nombre_corto_sap from b_sap_surtido_2 where txt_nombre_corto_sap<2000;
--Tiendas
select * from vw_b_sap_surtido where txt_nombre_corto_sap>=2000;
select m_merca from vw_b_sap_surtido where txt_nombre_corto_sap>=2000 group by m_merca;

select txt_nombre_corto_sap from b_sap_surtido where txt_nombre_corto_sap<2000 minus
select txt_nombre_corto_sap from b_sap_surtido_2 where txt_nombre_corto_sap<2000;
select * from vw_b_sap_surtido where txt_nombre_corto_sap>=2000;
select m_merca from vw_b_sap_surtido where txt_nombre_corto_sap>=2000 group by m_merca;

--Actualizar Vistas, las de VW_B_SAP
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VARIACIONES_SURTIDO"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_ADDITIONAL"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_EINA"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_EINE"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_MAKT"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_MAMT"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_MARA"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_MARM"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_MATERIAL_IMPUESTO"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_MEAN"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_MLEA"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_PCB_ARTICLE"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_PCB_ARTICLE_SITE"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_PVP_ARTICLE"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_PVP_ARTICLE_PRICELIST"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_PVP_ARTICLE_SITE"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_SURTIDO"','C'); end;
BEGIN DBMS_SNAPSHOT.REFRESH( '"ADM_RECON"."VW_B_SAP_WRF_BRANDS"','C'); end;