﻿--Control de versiones:
--v5 - 26.06.2019 Adaptación para produtos tipo pack y catch weight
--v6 - 06.09.2019 Optimización scripts para mejorar tiempos
--v7 - 11.09.2019 Optimizar scripts rendimiento (comentar productos nv y productos_ldp_view)

DELETE FROM ADM_RECON.B_SAP_INFORME_RECONCILIACION WHERE INFORME_SAP = 'IMPUESTOS';		
SELECT TO_CHAR
    (SYSDATE, 'HH24:MI:SS') "HORA DE INICIO"
     FROM DUAL;
UPDATE B_SAP_PROD_IMP_RECON_POST SET LEGACY_PRODUCT = trim(LEGACY_PRODUCT);
UPDATE B_SAP_PROD_IMP_RECON_POST SET ALAND = trim(ALAND);
UPDATE ADM_RECON.B_SAP_MATERIAL_IMPUESTO SET TAXIM = trim(TAXIM);

commit;

--I1 - Existe impuesto en SAP pero no en TRART
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'LEGACY_PRODUCT' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   B.LEGACY_PRODUCT AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   B.LEGACY_PRODUCT AS DATO_SAP, 
		   NULL AS DATO_LEGACY, 
		   NULL AS DATO_TRART,
		   'I1 - Existe impuesto en SAP pero no en TRART' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B
		WHERE NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_MATERIAL_IMPUESTO MI, ADM_RECON.B_SAP_CLASIFICACION_FISCAL CF
                           WHERE MI.KAPPL = CF.KAPPL AND MI.KSCHL = CF.KSCHL
                             AND MI.TAXIM = CF.TAXIM AND (MI.TAXIM = B.TAXM1 OR MI.TAXIM = B.TAXM2 OR MI.TAXIM = B.TAXM3 OR MI.TAXIM = B.TAXM4 OR MI.TAXIM = B.TAXM5
		OR MI.TAXIM = B.TAXM6 OR MI.TAXIM = B.TAXM7 OR MI.TAXIM = B.TAXM8 OR MI.TAXIM = B.TAXM9)
                             AND MI.ALAND = CF.ALAND AND MI.ALAND = B.ALAND 
                             AND MI.M_MERCA = B.LEGACY_PRODUCT)	  
	      AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = B.LEGACY_PRODUCT);
	COMMIT;
--I2 - Existe impuesto en TRART pero no en SAP
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'LEGACY_PRODUCT' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   MI.M_MERCA AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   NULL AS DATO_SAP, 
		   NULL AS DATO_LEGACY, 
		   MI.M_MERCA AS DATO_TRART,
		   'I2 - Existe impuesto en TRART pero no en SAP' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_MATERIAL_IMPUESTO MI, ADM_RECON.B_SAP_CLASIFICACION_FISCAL CF
        WHERE MI.KAPPL = CF.KAPPL AND MI.KSCHL = CF.KSCHL AND MI.TAXIM = CF.TAXIM AND MI.ALAND = CF.ALAND  
          AND NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B 
                           WHERE B.LEGACY_PRODUCT = MI.M_MERCA AND (MI.TAXIM = B.TAXM1 OR MI.TAXIM = B.TAXM2 OR MI.TAXIM = B.TAXM3 OR MI.TAXIM = B.TAXM4 OR MI.TAXIM = B.TAXM5
		OR MI.TAXIM = B.TAXM6 OR MI.TAXIM = B.TAXM7 OR MI.TAXIM = B.TAXM8 OR MI.TAXIM = B.TAXM9) AND MI.ALAND = B.ALAND)
	      AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = MI.M_MERCA);
	COMMIT;
--I3 - Existe impuesto en Legacy pero no en SAP (VENTA - NO Implantados)
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'LEGACY_PRODUCT' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   PTI.PTI_COD_PRODUCTO AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   NULL AS DATO_SAP, 
		   PTI.PTI_COD_PRODUCTO AS DATO_LEGACY, 
		   NULL AS DATO_TRART,
		   'I3 - Existe impuesto en Legacy pero no en SAP (VENTA - NO Implantados)' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_CLASIFICACION_FISCAL CLA, ADM_RECON.MGC_PROD_TIPO_IMPUESTOS PTI--, ADM_RECON.PRODUCTOS_LDP_VIEW_MIG LDP
     WHERE --PTI.PTI_COD_PRODUCTO = LDP.COD_N_MERCA AND LDP.MCA_IMPLANTADO = 'N' AND
	 
	 PTI.PTI_COD_IMPUESTO = CLA.M_COD_IMPUESTO AND PTI.PTI_COD_TIPO_IMPUESTO = CLA.M_COD_TIPO_IMPUESTO
       AND NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B 
                        WHERE B.LEGACY_PRODUCT = PTI.PTI_COD_PRODUCTO AND (CLA.TAXIM = B.TAXM1 OR CLA.TAXIM = B.TAXM2 OR CLA.TAXIM = B.TAXM3 OR CLA.TAXIM = B.TAXM4 OR CLA.TAXIM = B.TAXM5
		OR CLA.TAXIM = B.TAXM6 OR CLA.TAXIM = B.TAXM7 OR CLA.TAXIM = B.TAXM8 OR CLA.TAXIM = B.TAXM9) AND CLA.ALAND = B.ALAND)
       AND NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_MERCA_EXCLUSION WHERE PTI.PTI_COD_PRODUCTO = M_MERCA)
	   AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = PTI.PTI_COD_PRODUCTO);
	COMMIT; 
/*
--I4 - Existe impuesto en Legacy pero no en SAP (VENTA - Implantados)
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'LEGACY_PRODUCT' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   LDP.COD_N_MERCA AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   NULL AS DATO_SAP, 
		   LDP.COD_N_MERCA AS DATO_LEGACY, 
		   NULL AS DATO_TRART,
		   'I4 - Existe impuesto en Legacy pero no en SAP (VENTA - Implantados)' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_CLASIFICACION_FISCAL CLA, ADM_RECON.S_TIPO_IMPUESTO_ITEM STI, ADM_RECON.PRODUCTOS_LDP_VIEW_MIG LDP
     WHERE STI.COD_N_ITEM = LDP.COD_N_ITEM AND LDP.MCA_IMPLANTADO = 'S'
       AND STI.COD_N_IMPUESTO = CLA.M_COD_IMPUESTO AND STI.COD_N_TIPO_IMPUESTO = CLA.M_COD_TIPO_IMPUESTO
       AND NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B 
                        WHERE B.LEGACY_PRODUCT = LDP.COD_N_MERCA AND CLA.TAXIM = B.TAXM1 AND CLA.ALAND = B.ALAND)
       AND NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_MERCA_EXCLUSION WHERE LDP.COD_N_MERCA = M_MERCA)	  
	   AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = LDP.COD_N_MERCA);
*/

--I5 - Existe impuesto en Legacy pero no en SAP (NO VENTA)
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'LEGACY_PRODUCT' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   PNI.PNI_COD_PRODUCTO AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   NULL AS DATO_SAP, 
		   PNI.PNI_COD_PRODUCTO AS DATO_LEGACY, 
		   NULL AS DATO_TRART,
		   'I5 - Existe impuesto en Legacy pero no en SAP (NO VENTA)' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_CLASIFICACION_FISCAL CLA, ADM_RECON.MGC_PRODNV_TIPO_IMPUESTOS PNI
     WHERE PNI.PNI_COD_IMPUESTO = CLA.M_COD_IMPUESTO AND PNI.PNI_COD_TIPO_IMPUESTO = CLA.M_COD_TIPO_IMPUESTO
       AND NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B 
                        WHERE B.LEGACY_PRODUCT = PNI.PNI_COD_PRODUCTO AND CLA.TAXIM = B.TAXM1 AND CLA.ALAND = B.ALAND)
	   AND NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_MERCA_EXCLUSION WHERE PNI.PNI_COD_PRODUCTO = M_MERCA)
	   AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = PNI.PNI_COD_PRODUCTO);
commit;
--I6 - MATNR diferente entre SAP y TRART
	INSERT INTO B_SAP_INFORME_RECONCILIACION
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'MATNR' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION,
		   A.LEGACY_PRODUCT AS MERCA,
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   A.MATNR AS DATO_SAP,
		   NULL AS DATO_LEGACY,
		   (CASE WHEN LENGTH(TRIM(C.MATNR)) = 4 THEN LPAD(SUBSTR(C.MATNR,1,4),18,'0') ELSE LPAD(SUBSTR(C.MATNR,1,5),18,'0') END) AS DATO_TRART,
		   'I6 - MATNR diferente entre SAP y TRART' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST A  
	  LEFT JOIN ADM_RECON.B_SAP_MATERIAL_IMPUESTO C ON (A.LEGACY_PRODUCT = C.M_MERCA)
		WHERE (TRIM(A.MATNR) <> (CASE WHEN LENGTH(TRIM(C.MATNR)) = 4 THEN LPAD(SUBSTR(C.MATNR,1,4),18,'0') ELSE LPAD(SUBSTR(C.MATNR,1,5),18,'0') END) OR TRIM(A.MATNR) IS NULL)
		   AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = A.LEGACY_PRODUCT);
	COMMIT;		   
--I7 - TAXM1 no existe en el catálogo de unidades de medida de SAP
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT  'IMPUESTOS' AS INFORME_SAP,
			'TAXM1' AS CAMPO_SAP,  
			SYSDATE AS TM_EJECUCION, 
			B.LEGACY_PRODUCT AS MERCA, 
			NULL AS CENTRO, 
			NULL AS PROVEEDOR, 
			B.TAXM1 || ' ' || B.ALAND AS DATO_SAP, 
			NULL AS DATO_LEGACY, 
			NULL AS DATO_TRART,
			'I7 - TAXM1 no existe en el catálogo de unidades de medida de SAP' AS TXT_ERROR    
	FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B
	WHERE (B.TAXM1 IS NULL OR NOT EXISTS (SELECT 1 FROM ADM_RECON.B_SAP_CLASIFICACION_FISCAL CF WHERE B.ALAND = CF.ALAND AND (CF.TAXIM = B.TAXM1 OR CF.TAXIM = B.TAXM2 OR CF.TAXIM = B.TAXM3 OR CF.TAXIM = B.TAXM4 OR CF.TAXIM = B.TAXM5
		OR CF.TAXIM = B.TAXM6 OR CF.TAXIM = B.TAXM7 OR CF.TAXIM = B.TAXM8 OR CF.TAXIM = B.TAXM9)))
	  AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = B.LEGACY_PRODUCT);	   
	COMMIT;
--I8 - TAXM1 diferente entre SAP y TRART
INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   B.ALAND AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   B.LEGACY_PRODUCT AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   B.TAXM1 AS DATO_SAP, 
		   NULL AS DATO_LEGACY, 
		   MI.TAXIM AS DATO_TRART,
		   'I8 - TAXM1 diferente entre SAP y TRART' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B, ADM_RECON.B_SAP_MATERIAL_IMPUESTO MI, ADM_RECON.B_SAP_CLASIFICACION_FISCAL CLA
     WHERE 
     B.LEGACY_PRODUCT = MI.M_MERCA
     AND trim(B.ALAND) = trim(MI.ALAND) 
       AND ((B.TAXM1 <> MI.TAXIM AND MI.KSCHL='TTX1') OR (B.TAXM2 <> MI.TAXIM AND MI.KSCHL='ZIGI') OR (B.TAXM3 <> MI.TAXIM AND MI.KSCHL='ZIP1') OR (B.TAXM4 <> MI.TAXIM AND MI.KSCHL='ZIP2') OR (B.TAXM1 <> MI.TAXIM AND MI.KSCHL='MWST')) 
	   AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = B.LEGACY_PRODUCT);	 
	COMMIT;
--I9 - TAXM1 diferente entre SAP y Legacy
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'TAXIM' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   B.LEGACY_PRODUCT AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   B.TAXM1 AS DATO_SAP, 
		   CLA.TAXIM AS DATO_LEGACY, 
		   NULL AS DATO_TRART,
		   'I9 - TAXM1 diferente entre SAP y Legacy (VENTA - NO Implantados)' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B, ADM_RECON.B_SAP_CLASIFICACION_FISCAL CLA, ADM_RECON.MGC_PROD_TIPO_IMPUESTOS PTI
           --LEFT JOIN ADM_RECON.PRODUCTOS_LDP_VIEW_MIG ON (PTI.PTI_COD_PRODUCTO = COD_N_MERCA AND MCA_IMPLANTADO = 'N') 
     WHERE PTI.PTI_COD_PRODUCTO = B.LEGACY_PRODUCT
	   AND PTI.PTI_COD_IMPUESTO = CLA.M_COD_IMPUESTO AND PTI.PTI_COD_TIPO_IMPUESTO = CLA.M_COD_TIPO_IMPUESTO
       AND B.ALAND = CLA.ALAND AND B.TAXM1 <> CLA.TAXIM
	   AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = B.LEGACY_PRODUCT);	   
	COMMIT;
/*
--I10 - TAXM1 diferente entre SAP y Legacy (VENTA - Implantados)	   
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'TAXIM' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   B.LEGACY_PRODUCT AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   B.TAXM1 AS DATO_SAP, 
		   CLA.TAXIM AS DATO_LEGACY, 
		   NULL AS DATO_TRART,
		   'I10 - TAXM1 diferente entre SAP y Legacy (VENTA - Implantados)' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B, ADM_RECON.B_SAP_CLASIFICACION_FISCAL CLA, ADM_RECON.S_TIPO_IMPUESTO_ITEM STI
           LEFT JOIN ADM_RECON.PRODUCTOS_LDP_VIEW_MIG LDP ON (STI.COD_N_ITEM = LDP.COD_N_ITEM AND LDP.MCA_IMPLANTADO = 'S') 
     WHERE LDP.COD_N_MERCA = B.LEGACY_PRODUCT
	   AND STI.COD_N_IMPUESTO = CLA.M_COD_IMPUESTO AND STI.COD_N_TIPO_IMPUESTO = CLA.M_COD_TIPO_IMPUESTO
       AND B.ALAND = CLA.ALAND AND B.TAXM1 <> CLA.TAXIM  
	   AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = B.LEGACY_PRODUCT);	
*/
   
--I11 - TAXM1 diferente entre SAP y Legacy (NO VENTA)
	INSERT INTO B_SAP_INFORME_RECONCILIACION  
	SELECT 'IMPUESTOS' AS INFORME_SAP,
		   'TAXM1' AS CAMPO_SAP,
		   SYSDATE AS TM_EJECUCION, 
		   B.LEGACY_PRODUCT AS MERCA, 
		   NULL AS CENTRO,
		   NULL AS PROVEEDOR,
		   B.TAXM1 AS DATO_SAP, 
		   CLA.TAXIM AS DATO_LEGACY, 
		   NULL AS DATO_TRART,
		   'I11 - TAXM1 diferente entre SAP y Legacy (NO VENTA)' AS TXT_ERROR
	  FROM ADM_RECON.B_SAP_PROD_IMP_RECON_POST B, ADM_RECON.B_SAP_CLASIFICACION_FISCAL CLA, ADM_RECON.MGC_PRODNV_TIPO_IMPUESTOS PNI
     WHERE PNI.PNI_COD_PRODUCTO = B.LEGACY_PRODUCT
	   AND PNI.PNI_COD_IMPUESTO = CLA.M_COD_IMPUESTO AND PNI.PNI_COD_TIPO_IMPUESTO = CLA.M_COD_TIPO_IMPUESTO
       AND B.ALAND = CLA.ALAND AND B.TAXM1 <> CLA.TAXIM
	   AND EXISTS (SELECT 1 FROM ADM_RECON.S_ARTICULO_MERCA WHERE FEC_D_ALTA = '01/01/2021' AND TXT_AUD_PROC_ALTA = 'TRART' AND M_MERCA_UOM = B.LEGACY_PRODUCT);
commit;
SELECT TO_CHAR
    (SYSDATE, 'HH24:MI:SS') "HORA FIN"
     FROM DUAL;
	  
 

